<?php
chdir(dirname(__FILE__) .'/../..');

define('A_PLUGIN', 'minstockmatrix');
//TODO define (A_MODE, A_MODE_FRONT, A_MODE_ADMIN)
ini_set('error_reporting', E_ALL);
ini_set('register_globals', 0);
ini_set('magic_quotes_gpc', 0);
ini_set('mbstring.internal_encoding', 'utf-8');
ini_set('mbstring.func_overload', 2);
ini_set('include_path', 'system/pear/'. PATH_SEPARATOR . ini_get('include_path'));
setlocale(LC_COLLATE, 'ru_RU.UTF8');
setlocale(LC_CTYPE, 'ru_RU.UTF8');
require_once("system/framework/db.php");
//TODO require_once (functions, observer, cache)
require_once("config.php");
//TODO __autoload

class A {
	static $DB;
	//TODO CACHE, AUTH, OBSERVER, OPTIONS
	static $DOMAIN;
	static $DOMAINNAME;
	static $LANG = 'ru';
	static $STRUCTURE;

	static function ini () {
		A::$DB = A_DB::getInstance();
		//TODO get global options: SELECT var,value FROM _options
		//TODO CACHE, OBSERVER
	}
	//TODO getSystem
}
A::ini();

$domains = A::$DB->getAll('SELECT * FROM _domains');
foreach ( $domains as $domain ) {
	A::$DOMAIN = $domain['name'];
	A::$DOMAINNAME = $domain['domain'];

	//TODO get domain options: SELECT var,value FROM {$domain['name']}_options WHERE item = ''
	$structures = A::$DB->getAll("SELECT * FROM {$domain['name']}_structures WHERE plugin = ?", array(A_PLUGIN));
	foreach ( $structures as $structure ) {
		A::$STRUCTURE = "{$domain['name']}_structure_{$structure['name']}";

		//TODO get structure options: SELECT var,value FROM {$domain['name']}_options WHERE item = ? -- getOptions(A::$STRUCTURE)
		process();
	}
}

function process () {
	global $A_DBCONFIG;
	$db = new mysqli($A_DBCONFIG['host'],$A_DBCONFIG['user'],$A_DBCONFIG['password'],$A_DBCONFIG['name']);
	$db->query("SET NAMES 'utf8'");
	$STRUCTURE_stock = A::$STRUCTURE .'_stock';
	$STRUCTURE_stock_product = A::$STRUCTURE .'_stock_product';

	$soap1_wsdl_url = 'http://80.243.5.124:10080/v82/ws/exchange.1cws?wsdl';
	$soap2_wsdl_url = 'http://80.243.5.124:10080/v82/ws/Site.1cws?wsdl';
	$soap_login = 'cosmedelur';
	$soap_password = 'W1`+wZ}I%f';
	$soap1 = new SoapClient($soap1_wsdl_url, array('login'=>$soap_login, 'password'=>$soap_password, 'features'=>SOAP_SINGLE_ELEMENT_ARRAYS, 'cache_wsdl'=>WSDL_CACHE_NONE));
	$soap2 = new SoapClient($soap2_wsdl_url, array('login'=>$soap_login, 'password'=>$soap_password, 'features'=>SOAP_SINGLE_ELEMENT_ARRAYS, 'cache_wsdl'=>WSDL_CACHE_NONE));

	// GET CURRENT QUANTITY FROM 1C AND UPDATE THE MATRIX

	$soap_gpc_result = $soap2->GetProductCosmedel(array('SkladIDcosm'=>'', 'Codes'=>'', 'Quantity'=>''));
	//print_r($soap_gpc_result);
	if ( empty($soap_gpc_result->return) ) exit(); //TODO $soap_gpc_result->Error

	$reset_cq_stmt = $db->prepare("UPDATE $STRUCTURE_stock_product SET current_qty = 0");
	$reset_cq_stmt->execute(); //TODO or die
	echo "reset_cq: {$reset_cq_stmt->affected_rows} rows\n";
	$reset_cq_stmt->close();

	$stock_map = array();
	$upsert_cq_inserted_rows = 0;
	$upsert_cq_updated_rows = 0;
	$upsert_cq_stmt = $db->prepare("INSERT INTO $STRUCTURE_stock_product (stock_odines_code, product_article, current_qty) VALUES (?, ?, ?) ON DUPLICATE KEY UPDATE current_qty = ?");
	$upsert_cq_stmt->bind_param('ssii', $stock_odines_code, $product_article, $current_qty, $current_qty);
	foreach ( $soap_gpc_result->return->Value as $value ) {
		$stock_odines_code = $value->SkladID1С;
		$stock_cosmedel_code = $value->SkladIDcosm;
		$product_article = $value->NomCode;
		$current_qty = $value->Count;
		$stock_map[$stock_odines_code] = $stock_cosmedel_code;
		$upsert_cq_stmt->execute(); //TODO or die
		if ( $upsert_cq_stmt->affected_rows == 2 ) ++$upsert_cq_updated_rows;
		else $upsert_cq_inserted_rows += $upsert_cq_stmt->affected_rows;
	}
	echo "upsert_cq: $upsert_cq_inserted_rows/$upsert_cq_updated_rows rows inserted/updated\n";
	$upsert_cq_stmt->close();

	// UPDATE STOCK/SHOP INFO

	$upsert_cc_inserted_rows = 0;
	$upsert_cc_updated_rows = 0;
	$upsert_cc_stmt = $db->prepare("INSERT INTO $STRUCTURE_stock (odines_code, cosmedel_code) VALUES (?, ?) ON DUPLICATE KEY UPDATE cosmedel_code = ?");
	$upsert_cc_stmt->bind_param('sss', $odines_code, $cosmedel_code, $cosmedel_code);
	foreach ( $stock_map as $odines_code=>$cosmedel_code ) {
		$upsert_cc_stmt->execute(); //TODO or die
		if ( $upsert_cc_stmt->affected_rows == 2 ) ++$upsert_cc_updated_rows;
		else $upsert_cc_inserted_rows += $upsert_cc_stmt->affected_rows;
	}
	echo "upsert_cc: $upsert_cc_inserted_rows/$upsert_cc_updated_rows rows inserted/updated\n";
	$upsert_cc_stmt->close();

	// GET MINIMAL QUANTITY FROM 1C AND UPDATE THE MATRIX

	$soap_gm_result = $soap1->GetMatrix(array('IDStore'=>'', 'IDItem'=>''));
	//print_r($soap_gm_result);
	if ( !empty($soap_gm_result->Error) ) exit(); //TODO $soap_gm_result->Error

	$reset_mq_stmt = $db->prepare("UPDATE $STRUCTURE_stock_product SET minimal_qty = 0");
	$reset_mq_stmt->execute(); //TODO or die
	echo "reset_mq: {$reset_mq_stmt->affected_rows} rows\n";
	$reset_mq_stmt->close();

	$upsert_mq_inserted_rows = 0;
	$upsert_mq_updated_rows = 0;
	$upsert_mq_stmt = $db->prepare("INSERT INTO $STRUCTURE_stock_product (stock_odines_code, product_article, minimal_qty) VALUES (?, ?, ?) ON DUPLICATE KEY UPDATE minimal_qty = ?");
	$upsert_mq_stmt->bind_param('ssii', $stock_odines_code, $product_article, $minimal_qty, $minimal_qty);
	foreach ( $soap_gm_result->return->Value as $value ) {
		$stock_odines_code = $value->IDStore;
		foreach ( $value->MatrixLines as $item ) {
			$product_article = $item->IDItem;
			$minimal_qty = $item->Quantity;
			$upsert_mq_stmt->execute(); //TODO or die
			if ( $upsert_mq_stmt->affected_rows == 2 ) ++$upsert_mq_updated_rows;
			else $upsert_mq_inserted_rows += $upsert_mq_stmt->affected_rows;
		}
	}
	echo "upsert_mq: $upsert_mq_inserted_rows/$upsert_mq_updated_rows rows inserted/updated\n";
	$upsert_mq_stmt->close();

	// CALCULATE THE NEXT ORDERS AND PUT THEM TO 1C

	$calc_oq_stmt = $db->prepare("UPDATE $STRUCTURE_stock_product SET next_ordered_qty = IF(minimal_qty > current_qty, minimal_qty - current_qty, 0)");
	$calc_oq_stmt->execute(); //TODO or die
	echo "calc_oq: {$calc_oq_stmt->affected_rows} rows\n";
	$calc_oq_stmt->close();

	$stock_order_lines = array();
	$select_oq_fetched_rows = 0;
	$select_oq_stmt = $db->prepare("SELECT cosmedel_code, product_article, next_ordered_qty FROM $STRUCTURE_stock_product JOIN $STRUCTURE_stock ON stock_odines_code = odines_code WHERE next_ordered_qty > 0 AND stock_odines_code IN (SELECT stock_odines_code FROM $STRUCTURE_stock_product WHERE ordered_qty <> next_ordered_qty)");
	$select_oq_stmt->execute(); //TODO or die
	$select_oq_stmt->bind_result($cosmedel_code, $product_article, $next_ordered_qty);
	while (	$select_oq_stmt->fetch() ) {
		if ( !isset($stock_order_lines[$cosmedel_code]) ) $stock_order_lines[$cosmedel_code] = array();
		$stock_order_lines[$cosmedel_code][] = array('Code'=>$product_article, 'Quantity'=>$next_ordered_qty);
		++$select_oq_fetched_rows;
	}
	echo "select_oq: {$select_oq_fetched_rows} rows\n";
	$select_oq_stmt->close();

	$order_datetime = gmdate(DATE_ATOM);
	foreach ( $stock_order_lines as $cosmedel_code=>$order_lines ) {
		$soap_poc_params = array('Order'=>array('Date'=>$order_datetime, 'SkladIDcosm'=>$cosmedel_code, 'OrderLines'=>$order_lines));
		//print_r($soap_poc_params);
		$soap_poc_result = $soap1->PutOrderCosmedel($soap_poc_params);
		//print_r($soap_poc_result);
	}

	$reset_oq_stmt = $db->prepare("UPDATE $STRUCTURE_stock_product SET ordered_qty = next_ordered_qty, next_ordered_qty = NULL");
	$reset_oq_stmt->execute(); //TODO or die
	echo "reset_oq: {$reset_oq_stmt->affected_rows} rows\n";
	$reset_oq_stmt->close();
}
