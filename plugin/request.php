<?php

class MinStockMatrixPlugin_Request extends A_Request {

	function Action ($action) {
		switch ( $action ) {
			case 'getCurrentOrder': $data = $this->getCurrentOrder(); break;
			case 'getProducts':     $data = $this->getProducts();     break;
		}
		if ( isset($data) ) {
			header('Content-Type: application/json');
			echo json_encode($data);
		} else {
			A::NotFound();
		}
	}

	private function getCurrentOrder () {
		if ( !isset($_GET['shopId']) ) return;
		$cosmedel_code = $_GET['shopId'];

		$rows = A::$DB->getAll('SELECT * FROM '.STRUCTURE.'_stock_product WHERE stock_odines_code IN (SELECT odines_code FROM '.STRUCTURE.'_stock WHERE cosmedel_code = ?) AND ordered_qty > 0', array($cosmedel_code));
		if ( empty($rows) ) return;

		$json = array();
		foreach ( $rows as $row ) $json[] = array('product_article'=>$row['product_article'], 'quantity'=>intval($row['ordered_qty']));
		return array('order'=>$json);
	}

	private function getProducts () {
		if ( !isset($_GET['shopId']) ) return;
		$cosmedel_code = $_GET['shopId'];
		$more_than_zero = !$_GET['includeZero'];
		$include_stock = (bool) $_GET['includeStock'];

		$sql = 'SELECT product_article, current_qty';
		if ( $include_stock ) $sql .= ", (SELECT iscount FROM cosmedelru_ru_shoplite_catalog WHERE art = product_article AND active = 'Y' AND owner = 0) AS iscount"; //FIXME construct cosmedelru_ru_shoplite_catalog from variables
		$sql .= ' FROM '.STRUCTURE.'_stock_product';
		$sql .= ' WHERE stock_odines_code IN (SELECT odines_code FROM '.STRUCTURE.'_stock WHERE cosmedel_code = ?)';
		if ( $more_than_zero ) $sql .= ' AND current_qty > 0';

		$rows = A::$DB->getAll($sql, array($cosmedel_code));
		if ( empty($rows) ) return;

		$json = array();
		foreach ( $rows as $row ) {
			$product_json = array('article'=>$row['product_article'], 'shop_quantity'=>intval($row['current_qty']));
			if ( $include_stock ) $product_json['stock_quantity'] = intval($row['iscount']);
			$json[] = $product_json;
		}
		return array('products'=>$json);
	}

}

A::$REQUEST = new MinStockMatrixPlugin_Request;
