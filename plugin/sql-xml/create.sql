DROP TABLE IF EXISTS `{structure}_stock`;
CREATE TABLE `{structure}_stock` (
	`odines_code`   VARCHAR(50) NOT NULL,
	`cosmedel_code` VARCHAR(50),

	PRIMARY KEY (`odines_code`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `{structure}_stock_product`;
CREATE TABLE `{structure}_stock_product` (
	`stock_odines_code` VARCHAR(50) NOT NULL,
	`product_article`   VARCHAR(50) NOT NULL,
	`current_qty`       INT(11)     NOT NULL DEFAULT 0,
	`minimal_qty`       INT(11)     NOT NULL DEFAULT 0,
	`ordered_qty`       INT(11)     NOT NULL DEFAULT 0,
	`next_ordered_qty`  INT(11),

	PRIMARY KEY (`stock_odines_code`, `product_article`),
	INDEX       (`ordered_qty`),
	INDEX       (`next_ordered_qty`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
