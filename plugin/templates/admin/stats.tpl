{include file="_header.tpl"}
{if $rows}

<table class="grid">
<tr>
<th align="left">Код склада</th>
<th align="right">Заказано артикулов товара</th>
<th align="right">Заказано единиц товара</th>
</tr>
{section name=i loop=$rows}
<tr class="{cycle values="row0,row1"}">
<td>{$rows[i].stock_odines_code}</td>
<td align="right">{$rows[i].products_count}</td>
<td align="right">{$rows[i].items_count}</td>
</tr>
{/section}
</table>

{else}
<div class="box">Нет заказов.</div>
{/if}
{include file="_footer.tpl"}
