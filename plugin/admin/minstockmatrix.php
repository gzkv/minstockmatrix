<?php

class MinStockMatrixPlugin_Admin extends A_MainFrame {

	function __construct () {
		parent::__construct("stats.tpl");
	}

	function createData () {
		$rows = A::$DB->getAll('SELECT stock_odines_code, COUNT(*) AS products_count, SUM(ordered_qty) AS items_count FROM '.STRUCTURE.'_stock_product WHERE ordered_qty <> 0 GROUP BY stock_odines_code');
		$this->Assign('rows', $rows);
	}
}

A::$MAINFRAME = new MinStockMatrixPlugin_Admin;
